<x-guest-layout>
    <x-slot name="logo">

    </x-slot>

            <div class="container">
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                @if(session('success'))
                        <div class="alert alert-primary" role="alert">
                            {{session()->get('success')}}
                        </div>
                @endif
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">

                    <a class="btn btn-primary" role="button" href="{{ route('admin.users.create') }}">Add</a>


                </nav>
                    <div class="row justify-content-center">
                        <div class="col-md-12">

                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>name</th>
                                            <th>email</th>
                                            <th>opinions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($items as $user)

                                            @php /** @var \App\Models\User $user */ @endphp
                                            <tr>
                                                <td>

                                                    {{ $user->id }}

                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.users.edit',$user->id ) }}">
                                                        {{ $user->name }}
                                                    </a>
                                                </td>
                                                <td>{{ $user->email }}</td>
                                                <td>
                                                    <form method="POST" action="{{ route('admin.users.destroy',$user->id) }}">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-danger" >Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>


                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>


                        @if($items->total() > $items->count())
                            <div class="row">
                                  <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            {{ $items->links() }}
                                        </div>
                                    </div>
                                  </div>
                            </div>
                        @endif
            </div>
</x-guest-layout>
