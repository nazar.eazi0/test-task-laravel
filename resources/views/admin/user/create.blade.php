<x-guest-layout>
    <x-slot name="logo">

    </x-slot>


    <div class="container">
        @php /** @var \App\Models\User $items */ @endphp

        <h1>CRUD CREATE User</h1>

        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    {{ $error }}
                </div>
            @endforeach
        @endif

        @if(session('success'))
            <h2> {{session()->get('success')}} </h2>
        @endif
        <div class="col-lg-12 push-lg-4 personal-info">
            <form role="form" method="POST" action="{{ route('admin.users.store' ) }}" enctype="multipart/form-data">

                @csrf
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Avatar</label>
                    <div class="col-lg-9">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Upload</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input" >
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Name</label>
                    <div class="col-lg-9">
                        <input class="form-control" name="name" value="{{old('name')}}" type="text" " />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">email</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="email" name="email" value="{{old('email')}}"   />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Password</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="password" name="password"   />
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-lg-5 col-form-label form-control-label"></label>
                    <div class="col-lg-3">

                        <input type="submit" class="btn btn-primary" value="Create User" />
                    </div>
                </div>

            </form>
        </div>

    </div>


</x-guest-layout>
