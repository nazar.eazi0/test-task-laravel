<x-guest-layout>
    <x-slot name="logo">

    </x-slot>
    <div class="container">
        @php /** @var \App\Models\User $items */ @endphp
        <h1>CRUD EDIT User [{{ $items->id }}]</h1>

        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    {{ $error }}
                </div>
            @endforeach
        @endif

        @if(session('success'))
            <div class="alert alert-primary" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        <div class="col-lg-12 push-lg-4 personal-info">
            <form role="form" method="POST" action="{{ route('admin.users.update', $items->id ) }}" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Avatar</label>
                    @if($items->avatar)
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{ asset($items->avatar) }}" alt="Card image cap">

                        </div>
                    @endif
                    <div class="col-lg-9">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Upload</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input" >
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Name</label>
                    <div class="col-lg-9">
                        <input class="form-control" name="name" type="text" value="{{old('name',$items->name)}}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Email</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="email" name="email" value="{{old('email',$items->email)}}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label form-control-label">Reset Password</label>
                    <div class="col-lg-9">
                        <input class="form-control" type="password" name="password" " />
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-lg-5 col-form-label form-control-label"></label>
                    <div class="col-lg-3">

                        <input type="submit" class="btn btn-primary" value="Save Changes" />
                    </div>
                </div>

            </form>
        </div>

    </div>

</x-guest-layout>
