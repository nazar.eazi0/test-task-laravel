<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserObserver
{


    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function creating(User $user)
    {
        $this->heshPassword($user);
        $this->makeAvatar($user);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updating(User $user)
    {
        if ($user->isDirty('password'))
        {

            $this->heshPassword($user);

        }else
        {
            $user->password = $user->getOriginal('password');
        }

        $this->makeAvatar($user);


    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }

    /**
     * @param User $user
     */
    protected function heshPassword(User $user)
    {
        $user->password = Hash::make($user->password);
    }

    /**
     * @param User $user
     */
    protected function makeAvatar(User $user)
    {
        if ($user->avatar->isFile()) {

            $destinationPath = public_path('images\avatars');
            $fileName =   time() . '.jpg';

            $user->avatar->move($destinationPath, $fileName);
            $user->avatar =  "images/avatars/". $fileName;

        }
    }
}
