<?php


namespace App\Repositories;

use App\Models\User as Model;

/**
 * Class PostRepositories
 * @package Repositories
 */
class UserRepositories extends CoreRepositories
{
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     *
     *
     * @return mixed
     */
    public function getAllUser()
    {
        $columns = ['id','name','email'];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->paginate(5);


        return $result;
    }

    /**
     *
     * @param int $id
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }


}
