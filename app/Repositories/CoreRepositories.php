<?php


namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CoreRepositories
 * @package Repositories
 *
 * Репозиторий работи з сущностью.
 * Может видавать набори даних.
 * Не может создавать/изменять сущности.
 */

abstract class CoreRepositories
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * CoreRepositories constructor.
     */
    public function __construct()
    {
        $this->model =app($this->getModelClass());
    }

    /**
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     * @return \Illuminate\Contracts\Foundation\Application|Model|mixed
     */
    protected function startConditions()
    {
        return clone $this->model;
    }
}
