<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest as UserUpdateRequest;
use App\Models\User;
use App\Repositories\UserRepositories;


class UserControler extends Controller
{

    private $userRepositories;

    public function __construct()
    {

        $this->userRepositories = app(UserRepositories::class);
    }
    /**
     * Метод який виводить всіх юзерів з пагінацією 5 юзерів на одній сторінці
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = $this->userRepositories->getAllUser();

        return view('admin.user.index',compact('items'));
    }

    /**
     * Метод повіртає сторінку для створення нового юзера
     *
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.user.create');
    }

    /**
     * Метод додає юзера в базу даних хешує пароль і в випадку успіху редіректить на сторінку редагування
     *
     * @param UserStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserStoreRequest $request)
    {
        $data = $request->all();

        $item = User::create($data);


        if ($item)
        {
            return  redirect()->route('admin.users.edit',[$item->id])
                ->with(['success' => 'Create User']);
        }
        else
        {
            return back()->withErrors(['msd'=>'Error'])
                ->withInput();
        }
    }

    /**
     * Метод повертає сторінку редагування
     *
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $items = $this->userRepositories->getEdit($id);
        if (empty($items))
        {
            abort(404);
        }

        return view('admin.user.edit',compact('items'));
    }


    /**
     *  Метод редагує запис в базі даних змінює пароль і якщо все пройшло успішно редіректись на сторінку редагування
     *
     * @param UserUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $items = User::find($id);
        if (empty($items))
        {
            return back()
                ->withErrors(['msg' => "Юзер с ід={$id} не найдено"])
                ->withInput();
        }

        $data = $request->all();



        $result = $items->upDate($data);

        if ($result)
        {
            return redirect()
                ->route('admin.users.edit',$id)
                ->with(['success' => " Сохранено"]);
        }
        else
        {
            return back()
                ->withErrors(['msg' => "Error"])
                ->withInput();
        }
    }

    /**
     * Видаляє запис з бази даних якщо все добре редіректить на стронку редагування
     *
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = User::destroy($id);

        if ($result)
        {
            return redirect()
                ->route('admin.users.index',$id)
                ->with(['success' => "Удален юзер с id[$id] "]);
        }
        else
        {
            return back()
                ->withErrors(['msg' => "Error"]);

        }
    }
}
